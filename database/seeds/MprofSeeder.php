<?php

use Illuminate\Database\Seeder;
use App\Mprof;

class MprofSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Mprof::create([
            'name' => 'Administrator',
            'position' => 'Administrasi',
        ]);
    }
}
